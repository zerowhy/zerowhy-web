# Privacidade e Manipulação

---

## Aonde está a tecnologia em nossa vida?

Nos dias atuais (século XXI) a tecnologia aparece no nosso dia-a-dia de todas as maneiras: desde usar computadores e celulares até fazer compras em um mercado, sem mesmo que percebemos a autonomia e a dependência que possuímos a ela ou sua complexidade. Compras, trocas, mensagens, pesquisas e até as próprias relações familiares, que tanto prezavam do contato humano, se transformaram em conversões de um número infinito de dados, todos os dias.

## Serviços "gratuitos"

Muitas empresas privadas hoje em dia oferecem produtos como o Google Drive, YouTube, Microsoft Outlook e muitos outros serviços que podem ser utilizados sem nenhum custo monetário (pelo menos por um uso limitado). Em relação a isso, é possível questionar como essas empresas obtém lucro, até porque a grande maioria dos usuários não pagam pelo serviço de nenhuma maneira; as empresas conhecidas como "Big Tech" (Google, Amazon, Apple, Facebook e Microsoft) realizam esse tipo de estratégia (que funciona muito bem para atrair novos usuários!) e são as maiores empresas do planeta. A explicação desse fenômeno vêm de outro tipo de renda: o uso de dados coletados durante o seu uso do serviço para gerar lucro.

## Que tipos de dados são coletados hoje no seu dia-a-dia

Olhando um pequeno trecho na política de privacidade da Google:

> [...] As informações de atividades que coletamos podem incluir o seguinte:
>
> * termos que você pesquisa
> * vídeos que você assiste
> * visualizações e interações com conteúdo e anúncios
> * informações de voz e áudio quando você usa recursos de áudio
> * atividade de compra
> * pessoas com quem você se comunica ou compartilha conteúdo
> * atividades em sites e apps de terceiros que usam nossos serviços
> * histórico de navegação do Chrome que você sincronizou com a Conta do Google
>
> <https://policies.google.com/privacy?hl=pt-BR#infocollect>, acesso em 21/08/2021

Outras impresas realizam, em grande parte, a mesma coleta de dados. É importante citar também que este trecho da política de privacidade da Google não cobrem todos os dados coletados e como os mesmos são processados por eles. Outros exemplos de dados de grande impacto são informações de localização (onde você foi, com que você estava) e o tempo no qual você usa cada aplicativo no seu celular.

## Onde ficam os dados

Os dados coletados tem que ir para algum lugar. Popularmente chamamos esse lugar de "nuvem", que na verdade são grandes data centers (data = dado, center = centro: centro de dados) que são estruturas (gigantes!) que possuem muitos computadores (chamados de servidores), processando e guardando as informações de todos nós. Devido a algumas seletas empresas possuírem quase todo o mercado de servidores (Google, Amazon e Microsoft sendo os principais), todos os dados são armazenados e em confiança dessas empresas. Em consequência, todos os seus dados estão no controle de empresas de tecnologia pois além de empresas como a Google e Microsoft, pequenas e médias empresas também utilizam desses serviços (como sites do governo em alguns casos, que contém informações pessoais) que podem ser acessados e lidos livremente, pois na maioria das vezes os mesmos são armazenados sem qualquer criptografia. Esse acesso acaba aumentando o poder das grandes empresas e de governos, uma vez que uma grande quantidade de informações se concentram em um lugar único.

## Porque empresas querem dados

Como base para um mundo capitalista, a demanda de produtos e serviços para a rotação do capital se torna essencial e dessa forma, a oferta em constante mudança também toma suma importância, tanto para atrair olhares quanto para satisfazer os gostos do consumo. A teoria de um crescimento saudável de um mundo globalizado se inicia com o advento do aumento do poder de escolha, ora pela evolução dos parâmetros monetários ora pelo desenvolvimento dos meios de produção, onde a população consumidora passaria a exercer seu papel na requisição de demanda de uma forma mais crítica, buscando a constante melhora da sua oferta. Infelizmente, a realidade se distancia muito desse tipo de crescimento.

A adição tecnológica, além facilitar os meios por onde o consumo é realizado, surtiu influência em como este é realizado. No âmbito do aumento lucrativo e na quebra do crescimento saudável do capitalismo, as empresas iniciaram a chamada "personalização" de seus anúncios e campanhas, ao analisar o comportamento dos seus clientes e eliminar seu senso crítico, criando pontos de venda que satisfazem os seus gostos ou os induzem a alguma tendência de compra. Um conceito que pode ser aplicado neste contexto é o fetichismo de mercadoria, de Karl Marx: produtos são colocados em anúncios altamente customizados para um certo grupo demográfico e são mostrados como de alto valor agregado e de baixo volume (estoque), como por exemplo sapatos de luxo. Após isso, se cria um valor acima do valor real de venda (determinado pela quantidade de trabalho materializado no produto, de acordo com Marx).

Outro uso extremamente comum de todos esses dados é no meio físico: a construção de novas lojas ou mudança de preços, por exemplo. Empresas especializadas em usar esses dados mostram em gráficos, mapas e mais meios de comunicação informações relevantes (como fluxo de pessoas, média de quanto uma pessoa gasta na região, etc.) e são contratadas para realizar uma pesquisa com a intenção de encontrar possíveis regiões e estratégias para maximizar seus lucros com a loja.

Empresas que não realizam marketing digital também tem grande incentivo para realizar a coleta de dados, mesmo que não seja tão transparente. Principalmente no mercado de aplicativos de celular, é muito comum que este seja gratuito para uso e que, ao abrir o aplicativo, apareça um pedido para você concordar com os termos de privacidade. Ao concordar, o usuário permite a coleta de várias infomações individuais identificáveis que, em suma, são vendidos para quem pagar mais.

## O Estado e os dados

O Estado também tem grande interesse nos dados gerados por todos nós. Da mesma maneira que empresas privadas, os governos também utilizam dessas informações para realizar análises de mercado, a fim de realizar melhores escolhas, como e onde investir dinheiro público, entre outros.

Essa entidade, porém, possui muito mais informações que empresas privadas, em certos casos. Os Estados Unidos é um país onde grande parte das empresas da "Big Tech" estão sediadas e, devido a isso, caem sob a regulamentação norte-americana. Dessa forma, em caso de pedido legal, essas empresas devem ceder informações pessoais de usuários de seus serviços que, após o acesso, são utilizados para monitorar populações de outros países e a própria. Esse fator, combinado com o fato de que o financiamento direcionado a setores de inteligência (como a NSA, a National Security Agency nos EUA) é elevado, constitui um sistema por vários especialistas trabalhando, em "defender a nação". Apesar de que sua missão pública seja do interesse da população, muitas das ações dessa agências de inteligência são realizadas de forma clandestina, sem exposição pública. A NSA é notória nesse tipo de ação devido aos vazamentos que Edward Snowden, um ex-contratado da NSA, liberou de ações que essa agência realizava em nações estrangeiras e na própria nação.

Com base nisso, é possível concluir que governos são extremamente capazes de influenciar e monitorar toda a população em nível geral. Esse nível de poder é extremamente útil em casos de revoltas populares, por exemplo, podendo monitorar grupos contra o Estado e seus próximos ataques.

Infelizmente, alguns países atualmente chegam ao extremo e censuram todo conteúdo antigovernamental, como a China e a Coreia do Norte.

## O inevitável vazamento dos dados

Os dados são altamente valiosos para invasores de sistema (os famosos *hackers*), pois podem utilizá-los para ataques de engenharia social (por exemplo um email dizendo que é seu banco e que demanda um pagamento, o famoso ataque *phishing*), por uso direto (como uso de informação bancária para roubar fundos) ou para a venda dos mesmos à outros interessados (usualmente pela rede Onion, famosa *dark web*).

Infelizmente, computadores não são perfeitos; até os mais complexos e seguros sistemas do mundo não são impenetráveis e, como a recompensa de invasão desses sistemas é extremamente alto, o incentivo também será. Nos dias de hoje não é uma questão de se seus dados serão expostos mas quando serão.

Em 2021 já aconteceram inúmeros ataques à grandes sistemas, principalmente na forma de *ransomware*, um tipo de ataque que envolve restringir acesso aos dados de sistemas infectados e colocar um preço para obtê-los de volta; muitas vezes quando a empresa não paga os invasores acabam vendendo os dados para outros indivíduos, efetivamente vazando os dados. Exemplos neste ano incluem infraestrutura de petróleo estadunidense (*ransomware* que atacou a Colonial Pipeline Co.), vazamento de dados de sistemas de passaporte de vacinas da Irlanda, vazamento de dados pessoais de voluntários nos Jogos Olímpicos no Japão e muitos outros.

Para mais casos de ataques cibernéticos veja neste site, em inglês: <https://www.csis.org/programs/strategic-technologies-program/significant-cyber-incidents>

## Controle do fluxo de informações (notícias, resultado de pesquisas em mecanismos de busca, etc.)

Fora o controle das tendências de compra, a análise comportamental no ambiente virtual se relaciona em paralelo ao controle do fluxo de informações, criando ou até omitindo dados. As famosas Fake News se configuram apenas como a ponta do Iceberg desse controle de dados, em alguns mecanismos de busca, os resultados para mesmas pesquisas podem aparecer alterados, configurando possíveis censuras ou estratégias de apoio a mudança da opinião pública. Exemplos desse acontecimento são recorrentes em áreas sob a influência de governos que mandam a punho de ferro sobre o pensamento da população, como na China e o seu controle da liberdade na navegação na internet, induzido-os a uma aceitação idealizada dos mandos de seu governante (esta estratégia também pode ser usada em prol de evitar revoltas populares).

## Exemplos de uso mal intencionado

---

### China e o Crédito social

A fim de confirmar e assegurar a confiabilidade e a fidelidade de seus indivíduos, o governo Chinês implantou a população o sistema dos créditos sociais. O projeto iniciado em 2009 tem por base a listagem individual a qual, literalmente, segrega os habitantes do país por meio da avaliação aos seus costumes, além de impor-lhes um sistema de punição e recompensas por seus atos. Deste modo, ações casuais como faltar com compromisso em reservas de restaurantes, separar erroneamente o lixo e fumar foram classificadas como negativas e ao serem debitadas de uma pontuação numérica, podem impedir uma pessoa de realizar viagens, matricular suas crianças em certas escolas, conseguir empregos e se registrar em um quarto de hotel. Por outro lado, para aqueles cujas ações são aceitáveis (doações de sangue, trabalhos voluntariados e apologia ao governo), os benefícios de descontos em contas de luz, preferências em hospitais, uso livre de academias e maior atenção em bancos são garantidos.

Não tendo apenas o sistema governamental ao seu favor, o Crédito social também tem influência entre os seus próprios usuários. Como um incentivo para a população, o valor acumulado por certo indivíduo pode ser visto como um status social, além de que todos tem a livre permissão para reportar ações negativas e até atos de seguidores de uma religião não oficial chinesa, recebendo vantagens para tal.

Por fim, todos os dados que estão em teste nesse sistema são abertos ao público e qualquer um pode ter acesso as informações por meio da internet, uma clara violação a mais básica privacidade dos usuários que, por vezes, criticam este e muitos outros pontos.

### Facebook e Cambridge Analytica

Um exemplo que foi exposto em 2018 é o caso da Cambridge Analytica e o Facebook. Foram coletados dados de mais de 50 milhões de usuários da rede social por um aplicativo chamado "This Is Your Digital Life", que coletava informações por uma série de perguntas e construía um perfil psicológico do usuário e coletava os dados pessoais de seus amigos. Esses dados então foram usados predominantemente para assistência analítica do Ted Cruz e Donald Trump nas eleições de 2016. A empresa foi multada em 5 bilhões de dólares por violações de privacidade.

### LinkedIn 2021

Neste ano de 2021, ocorreu um vazamento de dados do serviço LinkedIn de mais de 700 milhões de usuários.

Os dados incluem nome completo, endereço de email, número de telefone, endereço residencial, registros de geolocalização, URL do perfil, gênero, histórico pessoal, profissional e links de contas em outras redes sociais.

Esses dados são vendidos pelos hackers na rede Onion (famosa dark web).
## O que fazer

Se estamos todos sujeitos a essa espionagem em massa do governo e todos os serviços gratuitos que usamos diariamente coletam tudo o que fazemos, para onde vamos fugir? É importante conhecer que hoje possuímos escolhas que podemos fazer para reduzir toda a essa coleta de informações sobre nós e diminuir o poder das grandes corporações. Para mitigar a espionagem do governo a tarefa é bem mais complicada, e infelizmente não vou cobrir esse tópico aqui, pois é muito extenso e complexo (afinal sou apenas uma pessoa atrás de um computador aprendendo coisas pela internet) mas muitas das coisas que se aplicam para evitar empresas auxiliam na privacidade a fim de evitar o governo.

### Perfil de risco

O seu perfil de risco é um fator fundamental para entender e aplicar mudanças na sua vida em relação a privacidade. Esse conceito é, em suma, o risco que você possui nas suas interações com a tecnologia. Este risco em específico está relacionada a quem você é na vida real, por exemplo: se você for um presidente, o seu perfil de risco é alto, pois você tem a tendência a ser atacado pelas vias tecnológicas, como a internet. Porém, caso você seja uma pessoa comum, o seu perfil de risco é baixo. Protestantes e grupos contra o governo são de um perfil de risco maior que a da pessoa comum. Esse fator tem importância pois nem sempre você precisa fazer algumas mudanças (principalmente as mais radicais!), evitando assim perder toda a sua conveniência, como será explicado em *Conveniência x privacidade*.

### Conveniência x privacidade

Ter privacidade nas redes infelizmente vêm com um custo alto: a conveniência no dia-a-dia. Por exemplo: imagine que você não quer mais utilizar mais nenhum serviço da Google ou de empresas semelhantes. Você teria dois opções após isso: apenas não utilizar mais nenhum armazenamento em nuvem ou criar o seu próprio servidor em sua casa e, para aprender a fazer isso, vai muitas e muitas horas de estudo e pesquisa, além de o resultado não ser indêntico aos usados anteriormente, muitas vezes com funções faltando. Ou imagine algo mais simples: não usar mapas da Google. Atualmente as alternativas (como o OpenStreetMaps) estão longe de alcançar o nível de facilidade e qualidade desses mapas. Mesmo parecendo muito difícil as mudanças, algumas pequenas escolhas ajudam muito em sua cybersegurança e privacidade como, por exemplo, usar outro navegador de internet que respeita sua privacidade com bloqueador de anúncios como o Brave. Sua escolha de quão longe você quer ir em nível de privacidade deve se basear em dois fatores:

* Qual meu perfil de risco?
* O quanto eu estou confortável de trocar privacidade por conveniência?

### O que você pode fazer agora

Essas são algumas sugestões de ações que você pode fazer agora.

#### Navegadores de internet

O mais básico e fácil é usar outro navegador de Internet. Chrome, Edge, Opera, Safari são apenas alguns exemplos de navegadores que você **NÃO** deve usar, pois todos eles possuem práticas que não vai te ajudar na busca por privacidade. Eu recomendo que você use ou o Brave (<https://brave.com>), já que este vem configurado de forma segura (só não esqueça de mudar o mecanismo de busca!). Para usá-lo, é simples: baixe o aplicativo pelo site, instale e mude o seu navegador padrão para o Brave: Vá para <https://brave.com/> e clique em "Download Brave". Se estiver no Windows, um download deve começar. Após o fim, clique em cima do arquivo para abri-lo. Siga os procedimentos para a instação e pronto! Não possuo experiência no macOS, então não vou poder te ajudar :(

Se quiser uma explicação de como mudar o navegador padrão no Windows 10: [https://support.microsoft.com/pt-br/windows/alterar-seu-navegador-padrão-no-windows-10-020c58c6-7d77-797a-b74e-8f07946c5db6](https://support.microsoft.com/pt-br/windows/alterar-seu-navegador-padr%C3%A3o-no-windows-10-020c58c6-7d77-797a-b74e-8f07946c5db6)

Nota especial para o *Microsoft Teams*: quando entrar no site, você pode acabar reparando que ele não carrega, ou mostra um texto pedindo para você autorizar o uso de cookies de terceiros. Para arrumar isso é simples: clique no símbolo do brave na barra de endereços e desabilite a proteção (Shields)

#### Mecanismos de busca

Mecanismos de busca é a proxima área que é simples de mudar: Google, Bing são os maiores e os piores para privacidade. Procure usar:

* DuckDuckGo: para ter os mesmos resultados do mecanismo de pesquisa Bing. <https://duck.com>
* Startpage: para ter os mesmos resultados do mecanismo de busca da Google. <https://startpage.com>
* Brave Search: para evitar ao máximo usar as pesquisas de grandes empresas.
No momento *(22/08/2021)* ainda não é possível mudar a língua da interface para português. <https://search.brave.com> Em todas esses mecanismos de busca, procure usar a função de região, pois ela ajuda em direcionar suas pesquisas para elas seres mais relevantes. Para estudandes do ensino médio que usam muito sites como Brasil Escola, etc., esses só aparecem se a região estiver selecionada como Brasil.

Infelizmente no Brave, o Google vem como motor de busca padrão. Para mudar, clique no hamburger (três linhas paralelas) no canto superior, vá em configurações, desça até achar algo como "Mecanismos de busca". Mude o motor de busca usado na barra de endereço e depois Clique em "Gerenciar mecanismos de busca", vá em "Mecanismos de busca padrão" e para mudar clique nos três pontos no mecanismo de busca escolhido e selecione em "Definir como padrão"

Para mudar o mecanismo de busca em outros navegadores, vá nesse site e procure o navegador que você está usando: <https://kinsta.com/pt/blog/como-mudar-o-mecanismo-de-pesquisa/>

#### Descentralização de dados

Você também pode evitar a coleta de dados e o poder de uma empresa única utilizando vários serviços web de forma distribuída. Por exemplo: ao invés de usar um e-mail para tudo, considere distribuir seu uso entre várias plataformas: Gmail, Microsoft Outlook, ProtonMail, Tutanota, entre outros.

#### Reduza seu uso de redes sociais

Essa dica é a mais comum e discutida (pelo menos acredito eu). Redes sociais hoje são utilizadas por bilhões de usuários e em alguns casos se tornam vicios, podendo levar até a grave problemas psicológicos (e isso não sou eu falando, são especialistas!). Empresas fazem uso de tudo que você publica, compartilha e "dá um joinha" para aprender os seus gostos e te prender usando a plataforma, além de mostrar anúncios personalizados. Twitter, YouTube, TikTok, Facebook, Instagram e muitos outros fazem isso e sem você perceber. Caso utilize essas plataformas para fins comerciais, é recomendado que use apenas para isso, e mais nada.

#### Aplicativo de mensagens

Essa é bem fácil de incorporar na sua vida. Aplicativos como o WhatsApp, Facebook Messenger, Mensagens por Twitter e Instagram, entre outros possuem graves riscos de privacidade ao usuário. A minha recomendação para realizar a transição é instalar o Signal. É um aplicativo gratuito e seguro, completo de funções, com aplicativo para computador. Infelizmente, devido à baixa adoção poucas pessoas usam atualmente, mas se você espalhar esse aplicativo para os seus amigos, poderá se comunicar com eles de uma forma mais segura! Site do aplicativo: <https://signal.org/> Link para download: <https://signal.org/install>

#### Gerenciador de senhas

Essa recomendação é de muita importância (mesmo!) em cybersegurança, mas leva mais tempo. Um gerenciador de senhas é de grande importância a fim de evitar problemas com vazamento de senhas (que é inevitável, como vimos anteriomente).

O trabalho de um gerenciador de senhas é simples: armazenar todas as suas senhas de uma forma segura para que você não precise decorar 1001 senhas. O objetivo final é utilizar uma senha diferente e gerada aleatoriamente para todos os sites. A mudança inicialmente não é tão fácil, mas vale muito apena. Vou demonstrar aqui o Bitwarden (<https://bitwarden.com/>), que acredito ser muito bom para iniciantes. Todas as suas senhas serão sincronizadas entre dispositivos, mas não se preucupe! O aplicativo foi auditado por experts de segurança e possui criptografia de alta qualidade.

Para começar é simples, baixe o aplicativo: Windows (provável que esse seja para você): <https://vault.bitwarden.com/download/?app=desktop&platform=windows>

macOS: <https://itunes.apple.com/app/bitwarden/id1352778147> Linux (AppImage): <https://vault.bitwarden.com/download/?app=desktop&platform=linux>

Para Windows, abra o arquivo e siga os procedimentos para instalá-lo no seu computador. Não tenho conhecimento na plataforma da Apple, infelizmente. Eu acredito em você, instale o Bitwarden.

Para todas as plataformas: Quando você abrir o aplicativo, será necessário criar uma conta e, para isso, é necessário um e-mail e senha segura. Cuidado! Não recomendo que utilize nenhuma senha que você anteriormente já usou, pois essa senha vai ser utilizada para acessar todas as outras senhas. Logo, crie uma senha: recomendo que seja maior que 15 caracteres, com símbolos especiais (@!#$%&, etc.) e que não seja fácil de adivinhar. Anote-a em um papel (no celular/computador não!) e guarde-o onde você precisar, caso você não consiga lembrar da nova senha com facilidade.

Agora é uma boa hora para você integrar o seu gerenciador de senhas com o navegador, para habilitar o autocompletar de senhas. Para isso, instale a extensão do Bitwarden: Brave/Chrome: <https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb> Firefox: <https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/>

Após instalar, vá para o canto superior direito (onde ficam as extensões por padrão) e clique no símbolo do Bitwarden (um escudo) e clique em log-in. Está pronto! Para usar é bem simples. Adicione uma senha no aplicativo, junto o nome do site/serviço e o URL (o link). Na próxima vez que for fazer login no site, clique no escudo e aparecerá sua conta. Clique-a e tudo será preenchido para você. É recomendado que você mude a senha de todos os lugares (é dificil, mas mude gradualmente) para uma senha aleatória. Ao adicionar sua conta no Bitwarden, tem um pequeno botão no campo de senha que gera senhas para você. Use e abuse! Também existe um aplicativo de celular, para que você possa acessar suas senhas: Apple: <https://itunes.apple.com/app/bitwarden-free-password-manager/id1137397744?mt=8> Android: <https://play.google.com/store/apps/details?id=com.x8bit.bitwarden>

Caso não esteja em seu computador, fora de casa, você pode acessar as senhas pela interface na web: <https://vault.bitwarden.com/> *De preferência em uma aba anônima, para evitar de deixar com login em um computador alheio. Se você não fizer isso, não esqueça de fazer log-out (sair da conta).*

### Notas

Há muitas coisas que você pode fazer sobre esse assunto. Eu recomendo que você pesquise por si só e procure aprender sobre computadores, controle de dados e todos esses tópicos. Você pode procurar sobre Linux, o sistema operacional gratuito que não te espiona (esqueceu que Windows é da Microsoft? Pois é), aplicativos como o Signal entre outros. Também existe um livro muito famoso que é bem interessante para a leitura: 1984, de George Orwell Se quiser aprender sobre a maior agência de espionagem do mundo, a NSA (National Security Agency) dos EUA, o artigo da Wikipedia tem muitas informações (está em inglês, a versão em português falta conteúdo): <https://en.wikipedia.org/wiki/National_Security_Agency> Existe uma plataforma de vídeo que respeita sua privacidade, uma alternativa para o YouTube: o Odysee <https://odysee.com>

## Precisa de ajuda?

As vezes parece que estamos presos em um buraco e por mais que tentamos só afundamos mais. Você pode tentar pesquisar na internet o seu problema, ou me contatar :)

A melhor maneira de entrar em contato comigo é pelo Element, um aplicativo de mensagens e chamada que protege sua privacidade. Para usar é simples, vá ao site <https://app.element.io/#/welcome>, clique em "Criar Conta". Escolha um nome de usuário, senha e e-mail e clique em "Registre-se" e continue seguindo o que aparece na tela. Você precisará autenticar o email. Após autenticar o e-mail, clique em entrar e faça log-in normalmente. Pronto! Agora só basta me mandar uma mensagem. Clique nesse link e entre no grupo criado: <https://matrix.to/#/!DfDznPnoDqPRGTqnKx:matrix.org?via=matrix.org> Clique em continuar, depois Element. Terá uma escolha: você pode baixar o aplicativo ou continuar usando pelo navegador (caso esteja indeciso, o navegador é o mais fácil). Clique em entrar na discussão e pronto! Agora só mandar sua mensagem.

---

> *Frase em latim, da inconfidência mineira que pode ser aplicada nesse contexto:* *"Libertas quæ sera tamen"* *Tradução: "Liberdade ainda que tardia"*

> *“Under observation, we act less free, which means we effectively are less free.”*
>
> *Tradução: "Sob observação, nós agimos com menos liberdade, o que significa que efetivamente somos menos livres."* *― Edward Snowden*
>
> *<https://pt.wikipedia.org/wiki/Edward_Snowden>*
